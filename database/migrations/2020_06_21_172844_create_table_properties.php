<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('penjual_id')->nullable();
            $table->unsignedBigInteger('kategori_id')->nullable();
            $table->string('judul')->nullable();
            $table->string('type')->nullable();
            $table->text('alamat')->nullable();
            $table->text('deskripsi')->nullable();
            $table->bigInteger('price')->nullable();
            $table->string('luas_bangunan')->nullable();
            $table->string('luas_tanah')->nullable();
            $table->string('kamar_tidur')->nullable();
            $table->string('kamar_mandi')->nullable();
            $table->string('in_active')->nullable();
            
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('penjual_id')
                ->references('id')
                ->on('penjuals')
                ->onUpdate('cascade');

            $table->foreign('kategori_id')
                ->references('id')
                ->on('categories')
                ->onUpdate('cascade');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_properties');
    }
}
