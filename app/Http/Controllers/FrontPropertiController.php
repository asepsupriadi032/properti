<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Properties;

class FrontPropertiController extends Controller
{
    public function beranda(){
    	$properti = Properties::get();
    	// dd($properti);
    	$menu = 'beranda';
    	return view('front.beranda', compact(['properti','menu']));
    }

    public function propertiDetail($id){
    	$properti = Properties::where('id',$id)
                ->firstOrFail();
    	return view('front.properti-detail', compact(['properti']));
    }
}
