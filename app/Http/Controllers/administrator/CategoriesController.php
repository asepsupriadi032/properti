<?php

namespace App\Http\Controllers\administrator;

use App\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Kategori</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>List</span>
                </li>
            </ul>';
        $title = "Kategori - List";
        $menu = "kategori";
        $kategori = Categories::get();
        return view('admin.kategori', compact(['breadcrumb','title','menu','kategori']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Kategori</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Tambah Data</span>
                </li>
            </ul>';
        $title = "Kategori - Tambah";
        $menu = "kategori";
        return view('admin.kategori-add', compact(['breadcrumb','title','menu']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori = new Categories();
        $data = $request->only([
            'kategori',
            'deskripsi'
        ]);
        // $data['icon']   = $request->icon->store('public/corporates');
        $kategori->fill($data);

        $kategori->save();

        return redirect(route('administrator.kategori.index'))->with('status','Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Kategori</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit Data</span>
                </li>
            </ul>';
        $title = "Kategori - Edit";
        $menu = "kategori";
        $kategori = Categories::where('id',$id)
                ->firstOrFail();
        return view('admin.kategori-edit', compact(['breadcrumb','title','menu','kategori']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori = Categories::find($id);
        $data = $request->only([
            'kategori',
            'deskripsi'
        ]);
        // $data['icon']   = $request->icon->store('public/corporates');
        $kategori->fill($data);

        $kategori->save();

        return redirect(route('administrator.kategori.index'))->with('status','Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Categories::find($id);
        $kategori->delete();
         return redirect(route('administrator.kategori.index'))->with('status','Berhasil');
    }
}
