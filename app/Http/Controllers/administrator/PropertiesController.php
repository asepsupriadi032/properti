<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Properties;
use App\Categories;
use App\Penjual;
use Illuminate\Http\Request;

class PropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Properti</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>List</span>
                </li>
            </ul>';
        $title = "Properti - List";
        $menu = "properti";
        $properti = Properties::get();
        // dd(Properties::get()->penjual);
        return view('admin.properti', compact(['breadcrumb','title','menu','properti']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Properti</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Tambah Data</span>
                </li>
            </ul>';
        $title = "Properti - Tambah";
        $menu = "properti";
        // dd(Properties::get()->penjual);
        $kategori = Categories::get();
        $penjual = Penjual::get();
        return view('admin.properti-add', compact(['breadcrumb','title','menu','kategori','penjual']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $properti = new Properties();
        $data = $request->only([
            'judul',
            'penjual_id',
            'kategori_id',
            'harga',
            'type',
            'alamat',
            'deskripsi',
            'luas_bangunan',
            'luas_tanah',
            'kamar_tidur',
            'kamar_mandi',
            'in_active',
        ]);
        $data['picture']   = $request->picture->store('public/properti');
        $properti->fill($data);

        $properti->save();

        return redirect(route('administrator.properti.index'))->with('status','Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function show(Properties $properties)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Properti</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit Data</span>
                </li>
            </ul>';
        $title = "Properti - Edit";
        $menu = "properti";
        $kategori = Categories::get();
        $penjual = Penjual::get();
        
        $properti = Properties::where('id',$id)
                ->firstOrFail();
        return view('admin.properti-edit', compact(['breadcrumb','title','menu','kategori','penjual','properti']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $properties = Properties::find($id);
        $data = $request->only([
            'judul',
            'penjual_id',
            'kategori_id',
            'harga',
            'type',
            'alamat',
            'deskripsi',
            'luas_bangunan',
            'luas_tanah',
            'kamar_tidur',
            'kamar_mandi',
            'in_active',
        ]);
        if($request->has('picture') && $request->picture != null){
            $data['picture'] = $request->picture->store('public/penjual');
            Storage::delete($properties->picture);
        }
        $properties->fill($data);

        $properties->save();

        return redirect(route('administrator.properti.index'))->with('status','Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $properti = Properties::find($id);
        $properti->delete();
         return redirect(route('administrator.properti.index'))->with('status','Berhasil');
    }
}
