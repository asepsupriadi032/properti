<?php

namespace App\Http\Controllers\administrator;

use App\Http\Controllers\Controller;
use App\Penjual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PenjualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Penjual</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>List</span>
                </li>
            </ul>';
        $title = "Penjual - List";
        $menu = "penjual";
        $penjual = Penjual::get();
        return view('admin.penjual', compact(['breadcrumb','title','menu','penjual']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Penjual</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Tambah Data</span>
                </li>
            </ul>';
        $title = "Penjual - Tambah";
        $menu = "penjual";
        return view('admin.penjual-add', compact(['breadcrumb','title','menu']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->poto);
        $penjual = new Penjual();
        $data = $request->only([
            'nama',
            'nik',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'pendidikan',
            'pekerjaan',
            'telp',
            'alamat'
        ]);
        $data['poto']   = $request->poto->store('public/penjual');
        $penjual->fill($data);

        $penjual->save();

        return redirect(route('administrator.penjual.index'))->with('status','Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function show(Penjual $penjual)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjual $penjual)
    {
        $breadcrumb = '<ul class="page-breadcrumb">
                <li>
                    <a href="#">Beranda</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Penjual</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit Data</span>
                </li>
            </ul>';
        $title = "Penjual - Edit";
        $menu = "penjual";
        return view('admin.penjual-edit', compact(['breadcrumb','title','menu','penjual']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjual $penjual)
    {
        $oldPicture = $penjual->poto;
        // dd($request->poto);
        $data = $request->only([
            'nama',
            'nik',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'pendidikan',
            'pekerjaan',
            'telp',
            'alamat'
        ]);
        if($request->has('poto') && $request->poto != null){
            $penjual->poto = $request->poto->store('public/penjual');
            Storage::delete($oldPicture);
        }
        $penjual->fill($data);

        $penjual->save();

        return redirect(route('administrator.penjual.index'))->with('status','Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penjual  $penjual
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjual $penjual)
    {
        $penjual->delete();
        return redirect(route('administrator.penjual.index'))->with('status','Berhasil');
    }
}
