<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Properties extends Model
{
    
    use SoftDeletes;
    protected $table = "properties";
    protected $guarded = [];


    public function penjual(){
        return $this->belongsTo(Penjual::class,'penjual_id');
    }
}
