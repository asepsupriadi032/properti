<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/properti/beranda');
});
Route::name('front.')->group(function(){
    Route::prefix('properti')->group(function(){
		Route::get('/','FrontPropertiController@beranda')->name('beranda');
		Route::get('/beranda','FrontPropertiController@beranda')->name('beranda');

		Route::get('/properti-detail/{id}','FrontPropertiController@propertiDetail')->name('properti-detail');

    });
});



Route::middleware(['auth'])->group(function(){
	Route::name('administrator.')->group(function(){
	    Route::prefix('admin')->group(function(){

	    	Route::resource('beranda','administrator\BerandaAdminController');
	    	Route::resource('kategori', 'administrator\CategoriesController');
	    	Route::resource('penjual', 'administrator\PenjualController');
	    	Route::resource('properti', 'administrator\PropertiesController');
	    });
	});
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
