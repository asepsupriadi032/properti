@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-7 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{$menu}} Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form action="{{route('administrator.properti.update',$properti->id)}}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Judul</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-building font-green"></i>
                                </span>
                                <input type="text" class="form-control" name="judul" placeholder="Judul" value="{{$properti->judul}}"> </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Penjual</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user font-green"></i>
                                        </span>
                                        <select class="form-control" name="penjual_id">
                                            @foreach($penjual as $row)
                                                @if($row->id == $properti->penjual_id)
                                                    <option value="{{$row->id}}" selected="">{{$row->nama}} - {{$row->alamat}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{$row->nama}} - {{$row->alamat}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Kategori Properti</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="kategori_id">
                                            @foreach($kategori as $row)
                                                @if($row->id == $properti->kategori_id)
                                                    <option value="{{$row->id}}" selected="">{{$row->kategori}}</option>
                                                @else
                                                    <option value="{{$row->id}}">{{$row->kategori}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-money font-green"></i>
                                        </span>
                                        <input type="number" name="harga" class="form-control" placeholder="Harga" value="{{$properti->harga}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="type">
                                            <option value="jual" @if($properti->type == 'jual') selected @endif>Jual</option>
                                            <option value="beli" @if($properti->type == 'beli') selected @endif>Beli</option>
                                            <option value="sewa" @if($properti->type == 'sewa') selected @endif>Sewa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" rows="3" name="alamat">{{$properti->alamat}}</textarea>
                        </div>   
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" name="deskripsi">{{$properti->deskripsi}}</textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Luas Bangunan (M<sup>2</sup>)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-building font-green"></i>
                                        </span>
                                        <input type="text" name="luas_bangunan" class="form-control" value="{{$properti->luas_bangunan}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Luas Tanah (M<sup>2</sup>)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-map-o font-green"></i>
                                        </span>
                                        <input type="text" name="luas_tanah" class="form-control" value="{{$properti->luas_tanah}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kamar Tidur</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-hotel font-green"></i>
                                        </span>
                                        <input type="text" name="kamar_tidur" class="form-control" value="{{$properti->kamar_tidur}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kamar Mandi</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-male font-green"></i>
                                            <i class="fa fa-female font-green"></i>
                                        </span>
                                        <input type="text" name="kamar_mandi" class="form-control" value="{{$properti->kamar_mandi}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Gambar</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-file-picture-o font-green"></i>
                                    </span>
                                    <input type="file" name="picture" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                    <label>Status Aktif</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="in_active">
                                            <option value="active"@if($properti->in_active == 'active') selected @endif>active</option>
                                            <option value="in_active" @if($properti->in_active == 'in_active') selected @endif>in_active</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="reset" class="btn default">Cancel</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div
@endsection