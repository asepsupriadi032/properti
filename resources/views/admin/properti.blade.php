@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <a href="{{route('administrator.properti.create')}}" class="btn btn-success"><i class="fa fa-plus mr-1"></i>Tambah Data</a>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Penjual</th>
                            <th>Kategori</th>
                            <th>Tipe</th>
                            <th>Harga</th>
                            <th>Alamat</th>
                            <th>Photo</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Penjual</th>
                            <th>Kategori</th>
                            <th>Tipe</th>
                            <th>Harga</th>
                            <th>Alamat</th>
                            <th>Photo</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    @php ($i=1)
                    @foreach($properti as $row)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$row->judul}}</td>
                            <td>{{$row->penjual_id}}</td>
                            <td>{{$row->kategori_id}}</td>
                            <td>{{$row->type}}</td>
                            <td>{{$row->harga}}</td>
                            <td>{{$row->alamat}}</td>
                            <td><img src="{{$row->poto}}"></td>
                            <td>
                            <a href="{{route('administrator.properti.edit',$row->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            <a class="btn btn-danger" href="{{route('administrator.properti.destroy',$row->id)}}" onclick="event.preventDefault();
                                 document.getElementById('hapus-form').submit();">
                               <i class="fa fa-times"></i> Hapus
                            </a>
                            <form id="hapus-form" action="{{route('administrator.properti.destroy',$row->id)}}" method="post" style="display: none;">
                            @method('delete')
                                            @csrf
                            </form>
                            
                            </td>
                        </tr>
                    @php ($i++)
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>
@endsection