@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-7 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Kategori Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form action="{{route('administrator.kategori.store')}}" method="post">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Jenis Kategori</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-list-ul font-green"></i>
                                </span>
                                <input type="text" class="form-control" name="kategori" placeholder="Jenis Kategori"> </div>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" name="deskripsi"></textarea>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="reset" class="btn default">Cancel</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div
@endsection