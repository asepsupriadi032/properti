@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <a href="{{route('administrator.penjual.create')}}" class="btn btn-success"><i class="fa fa-plus mr-1"></i>Tambah Data</a>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Jenis Kelami</th>
                            <th>Telp</th>
                            <th>Alamat</th>
                            <th>Photo</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIK</th>
                            <th>Jenis Kelami</th>
                            <th>Telp</th>
                            <th>Alamat</th>
                            <th>Photo</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    @php ($i=1)
                    @foreach($penjual as $row)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$row->nama}}</td>
                            <td>{{$row->nik}}</td>
                            <td>{{$row->jenis_kelamin}}</td>
                            <td>{{$row->telp}}</td>
                            <td>{{$row->alamat}}</td>
                            <td><img src="{{$row->poto}}"></td>
                            <td>
                            <a href="{{route('administrator.penjual.edit',$row->id)}}" class="btn btn-warning">Edit</a>
                            <a class="btn btn-danger" href="{{route('administrator.penjual.destroy',$row->id)}}" onclick="event.preventDefault();
                                 document.getElementById('hapus-form').submit();">
                                Hapus
                            </a>
                            <form id="hapus-form" action="{{route('administrator.penjual.destroy',$row->id)}}" method="post" style="display: none;">
                            @method('delete')
                                            @csrf
                            </form>
                            
                            </td>
                        </tr>
                    @php ($i++)
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>
@endsection