@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <a href="{{route('administrator.kategori.create')}}" class="btn btn-success"><i class="fa fa-plus mr-1"></i>Tambah Data</a>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    @php ($i=1)
                    @foreach($kategori as $row)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$row->kategori}}</td>
                            <td>{{$row->deskripsi}}</td>
                            <td>
                            <a href="{{route('administrator.kategori.edit',$row->id)}}" class="btn btn-warning">Edit</a>
                            <a class="btn btn-danger" href="{{route('administrator.kategori.destroy',$row->id)}}" onclick="event.preventDefault();
                                 document.getElementById('hapus-form').submit();">
                                Hapus
                            </a>
                            <form id="hapus-form" action="{{route('administrator.kategori.destroy',$row->id)}}" method="post" style="display: none;">
                            @method('delete')
                                            @csrf
                            </form>
                            
                            </td>
                        </tr>
                    @php ($i++)
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>
@endsection