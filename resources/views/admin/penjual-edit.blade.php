@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-7 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Kategori Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form action="{{route('administrator.penjual.update',$penjual->id)}}" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user font-green"></i>
                                </span>
                                <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="{{$penjual->nama}}"> </div>
                        </div>
                        <div class="form-group">
                            <label>NIK</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-credit-card font-green"></i>
                                </span>
                                <input type="text" class="form-control" name="nik" placeholder="NIK" value="{{$penjual->nik}}"> </div>
                        </div>
                                
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-map-marker font-green"></i>
                                        </span>
                                        <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" value="{{$penjual->tempat_lahir}}"> </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar font-green"></i>
                                        </span>
                                        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir" value="{{$penjual->tanggal_lahir}}"> </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value="laki-laki"<?php if($penjual->jenis_kelamin == 'laki-laki') {?> selected <?php } ?>>Laki-laki</option>
                                <option value="perempuan" <?php if($penjual->jenis_kelamin=='perempuan') {?> selected <?php } ?>>Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pendidikan</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-graduation-cap font-green"></i>
                                </span>
                                <input type="text" name="pendidikan" class="form-control" placeholder="Pendidikan" value="{{$penjual->pendidikan}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label>Pekerjaan</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user font-green"></i>
                                </span>
                                <input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan" value="{{$penjual->pekerjaan}}">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label>Telephone</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-mobile-phone font-green"></i>
                                </span>
                                <input type="text" name="telp" class="form-control" placeholder="Nomor Telephone" value="{{$penjual->telp}}">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label>Photo</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-photo font-green"></i>
                                </span>
                                <input type="file" name="poto" class="form-control" placeholder="Nomor Telephone">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" rows="3" name="alamat"></textarea>
                        </div>

                    </div>
                    <div class="form-actions right">
                        <button type="reset" class="btn default">Cancel</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div
@endsection