@extends('admin.master-page')
@section('content')
<div class="row">
    <div class="col-md-7 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{$menu}} Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form action="{{route('administrator.properti.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Judul</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-building font-green"></i>
                                </span>
                                <input type="text" class="form-control" name="judul" placeholder="Judul"> </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Penjual</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user font-green"></i>
                                        </span>
                                        <select class="form-control" name="penjual_id">
                                            @foreach($penjual as $row)
                                            <option value="{{$row->id}}">{{$row->nama}} - {{$row->alamat}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Kategori Properti</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="kategori_id">
                                            @foreach($kategori as $row)
                                            <option value="{{$row->id}}">{{$row->kategori}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-money font-green"></i>
                                        </span>
                                        <input type="number" name="harga" class="form-control" placeholder="Harga">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="type">
                                            <option value="jual">Jual</option>
                                            <option value="beli">Beli</option>
                                            <option value="sewa">Sewa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" rows="3" name="alamat"></textarea>
                        </div>   
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" name="deskripsi"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Luas Bangunan (M<sup>2</sup>)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-building font-green"></i>
                                        </span>
                                        <input type="text" name="luas_bangunan" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Luas Tanah (M<sup>2</sup>)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-map-o font-green"></i>
                                        </span>
                                        <input type="text" name="luas_tanah" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kamar Tidur</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-hotel font-green"></i>
                                        </span>
                                        <input type="text" name="kamar_tidur" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kamar Mandi</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-male font-green"></i>
                                            <i class="fa fa-female font-green"></i>
                                        </span>
                                        <input type="text" name="kamar_mandi" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Gambar</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-file-picture-o font-green"></i>
                                    </span>
                                    <input type="file" name="picture" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                    <label>Status Aktif</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-arrows font-green"></i>
                                        </span>
                                        <select class="form-control" name="in_active">
                                            <option value="active">active</option>
                                            <option value="in_active">in_active</option>
                                        </select>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="reset" class="btn default">Cancel</button>
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div
@endsection